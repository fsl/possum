/*  systemnoise.cc

    Mark Jenkinson, FMRIB Image Analysis Group

    Copyright (C) 2004 University of Oxford  */

/*  CCOPYRIGHT  */

// Application for adding noise to the signal output from possum


#include <iostream>
#include <string>
#include <time.h>

#include "utils/options.h"
#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/miscprob.h"

using namespace std;
using namespace Utilities;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;

// The two strings below specify the title and example usage that is
//  printed out as the help or usage message

string title="systemnoise\nCopyright(c) 2004, University of Oxford (Mark Jenkinson)";
string examples="systemnoise [options] --in=<signal> --out=<signal> --sigma=<value>";


Option<bool> verbose(string("-v,--verbose"), false,
		     string("switch on diagnostic messages"),
		     false, no_argument);
Option<bool> help(string("-h,--help"), false,
		  string("display this message"),
		  false, no_argument);
Option<float> sigma(string("-s,--sigma"), 0.0,
		  string("set noise standard deviation (units of intensity)"),
		  false, requires_argument);
Option<string> inname(string("-i,--in"), string(""),
		  string("input signal (possum output matrix)"),
		  true, requires_argument);
Option<string> outname(string("-o,--out"), string(""),
		  string("output signal (possum matrix form)"),
		  true, requires_argument);
Option<int> seed(string("--seed"), 3725473,
		  string("input seed value for the sequence"),
		  false, requires_argument);

int nonoptarg;

////////////////////////////////////////////////////////////////////////////



int do_work(int argc, char* argv[])
{
  Matrix signal;
  signal=read_binary_matrix(inname.value());

  if (verbose.value()) { cout << "Sigma = " << sigma.value() << endl; }
  int seed_val=seed.value();
  if (seed.unset()){
    time_t loc;
    time(&loc);
    seed_val=loc%105634;
  }
  srand(seed_val);
  signal += normrnd(signal.Nrows(),signal.Ncols(),0,sigma.value());

  write_binary_matrix(signal,outname.value());

  return 0;
}

////////////////////////////////////////////////////////////////////////////

int main(int argc,char *argv[])
{

  Tracer tr("main");
  OptionParser options(title, examples);

  try {
    // must include all wanted options here (the order determines how
    //  the help message is printed)
    options.add(inname);
    options.add(outname);
    options.add(sigma);
    options.add(seed);
    options.add(verbose);
    options.add(help);

    nonoptarg = options.parse_command_line(argc, argv);

    // line below stops the program if the help was requested or
    //  a compulsory option was not set
    if ( (help.value()) || (!options.check_compulsory_arguments(true)) )
      {
	options.usage();
	exit(EXIT_FAILURE);
      }

  }  catch(X_OptionError& e) {
    options.usage();
    cerr << endl << e.what() << endl;
    exit(EXIT_FAILURE);
  } catch(std::exception &e) {
    cerr << e.what() << endl;
  }

  // Call the local functions

  return do_work(argc,argv);
}
