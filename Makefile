include ${FSLCONFDIR}/default.mk

PROJNAME   = possum
RUNTCLS    = Possum
XFILES     = possum spharm_rm signal2image pulse systemnoise \
             possum_sum b0calc possum_matrix tcalc
TESTXFILES = test_possum
SCRIPTS    = possumX possumX_postproc.sh generate_b0 generate_brain \
             generate_b0calc possum_interpmot.py possum_plot.py
MFILES     = read_pulse.m write_pulse.m

LIBS       = -lfsl-newimage -lfsl-miscmaths -lfsl-utils -lfsl-cprob \
             -lfsl-NewNifti -lfsl-znz

all: ${XFILES} ${MFILES}

possum: possum.o possumfns.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

pulse: pulse.o possumfns.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

possum_matrix: possum_matrix.o possumfns.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

%: %.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

matlabfiles:
	@if [ ! -d ${DESTDIR}/etc/matlab ] ; then ${MKDIR} -p ${DESTDIR}/etc/matlab ; ${CHMOD} -R g+w ${DESTDIR}/etc ; fi
	${CP} ${MFILES} ${DESTDIR}/etc/matlab
