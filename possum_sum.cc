/*  possum_sum.cc

    Ivana Drobnjak, FMRIB Image Analysis Group

    Copyright (C) 2006 University of Oxford  */

/*  CCOPYRIGHT  */

// Combining many different possum outputs into one (for use on a cluster)

#include <time.h>
#include <iostream>
#include <string>

#include "utils/options.h"
#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/miscprob.h"

using namespace std;
using namespace Utilities;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;

// The two strings below specify the title and example usage that is
//  printed out as the help or usage message

string title="possum_sum\nCopyright(c) 2006, University of Oxford (Ivana Drobnjak)";
string examples="possum_sum -i <inname> -o <outname> -n <nproc> ";


Option<bool> verbose(string("-v,--verbose"), false,
		     string("switch on diagnostic messages"),
		     false, no_argument);
Option<bool> help(string("-h,--help"), false,
		  string("display this message"),
		  false, no_argument);
Option<int> opt_nproc(string("-n,--nproc"), 1,
		  string("Number of processors"),
		  false, requires_argument);
Option<string> inname(string("-i,--in"), string(""),
		  string("input signal for one processor (possum output matrix)"),
		  true, requires_argument);
Option<string> outname(string("-o,--out"), string(""),
		  string("output signal: sum of all the processors (possum matrix form)"),
		  true, requires_argument);

int nonoptarg;

////////////////////////////////////////////////////////////////////////////



int do_work(int argc, char* argv[])
{
  Matrix signal, signal1;
  signal=read_binary_matrix(inname.value()+ num2str(0));
  int nproc=opt_nproc.value();
  for (int i=1;i<nproc;i++){
    signal1=read_binary_matrix(inname.value()+ num2str(i));
    if (verbose.value()) { cout << "proc name = " << inname.value()+ num2str(i) << endl; }
    signal +=signal1;
  }
  write_binary_matrix(signal,outname.value());

  return 0;
}

////////////////////////////////////////////////////////////////////////////

int main(int argc,char *argv[])
{

  Tracer tr("main");
  OptionParser options(title, examples);

  try {
    // must include all wanted options here (the order determines how
    //  the help message is printed)
    options.add(inname);
    options.add(outname);
    options.add(opt_nproc);
    options.add(help);
    options.add(verbose);

    nonoptarg = options.parse_command_line(argc, argv);

    // line below stops the program if the help was requested or
    //  a compulsory option was not set
    if ( (help.value()) || (!options.check_compulsory_arguments(true)) )
      {
	options.usage();
	exit(EXIT_FAILURE);
      }

  }  catch(X_OptionError& e) {
    options.usage();
    cerr << endl << e.what() << endl;
    exit(EXIT_FAILURE);
  } catch(std::exception &e) {
    cerr << e.what() << endl;
  }

  // Call the local functions

  return do_work(argc,argv);
}
