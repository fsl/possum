#!/bin/sh

#   POSSUM
#
#   Ivana Drobnjak & Mark Jenkinson, FMRIB Analysis Group
#
#   Copyright (C) 2005-2007 University of Oxford
#
#   SHCOPYRIGHT

subjdir=$1
nproc=$2

#$ -S /bin/sh
#$ -V
#$ -N p_possum
#$ -m ae

# For all the users POSSUMDIR will be empty and therefore automatically become FSLDIR
# For me POSSUMDIR is my FSLDEVDIR directory. This allows me to run POSSUM on the cluster 
# without having to make it stable and wait for a day or find way to run my binaries. 
# It just seemed the easiest way to get around this. It is in all POSSUM scripts so please
# leave it that way if possible.
if [ x${POSSUMDIR} = x ] ; then
   export POSSUMDIR=$FSLDIR
fi

run(){
 echo "$1" >> $2/possum.log
 $1 >> $2/possum.log 2>&1
 date >> $2/possum.log
}

echo Summing all signal from different proccesses into one total signal
run "${POSSUMDIR}/bin/possum_sum -i ${subjdir}/diff_proc/signal_proc_ -o ${subjdir}/signal -n $nproc " ${subjdir}

echo Converting the signal into the image
run "${POSSUMDIR}/bin/signal2image -i ${subjdir}/signal -o ${subjdir}/image -p ${subjdir}/pulse -a --homo " ${subjdir}

echo Removing intermediate files
if [ -e ${subjdir}/signal ]; then
      rm -rf ${subjdir}/diff_proc
    #rm -rf ${subjdir}/matrix
fi

echo Adding noise
n=sigma
m=0
if [ -e ${subjdir}/noise ]; then
  n=`cat ${subjdir}/noise | awk '{print $1 }'`
  m=`cat ${subjdir}/noise | awk '{print $2 }'`
fi

if [ `${FSLDIR}/bin/imtest ${subjdir}/image_homo` -eq 1 ]; then
   imcp image_homo image_abs
fi

fslmaths ${subjdir}/image_abs -Tmean ${subjdir}/image_mean
P98=`fslstats ${subjdir}/image_mean -P 98`
P02=`fslstats ${subjdir}/image_mean -P 2`
tresh=`echo "0.1 * $P98 + 0.9 * $P02 "|bc -l`
fslmaths ${subjdir}/image_mean -thr $tresh ${subjdir}/image_mean
medint=`fslstats ${subjdir}/image_mean -P 50`
dim1=`fslval ${subjdir}/image_abs dim1`
if [ $n = "snr" ]; then
  snr=$m
  if [ $snr != 0 ]; then
     #sigma=`echo " ${medint} / ( 2 * ${dim1} * ${snr} ) "| bc -l` #I worked this out ages ago.
     sigma=`echo " ${medint} / $snr " | bc -l`
     echo "sigma ${sigma} snr ${snr} medintensity ${medint}" > ${subjdir}/noise 
  else
     echo "snr  0" > ${subjdir}/noise
  fi
else
  sigma=$m
  if [ $sigma != 0 ]; then
     snr=`echo " ${medint} / ( 2 * ${dim1} * ${sigma} ) "| bc -l`
     echo "sigma $sigma snr $snr" > ${subjdir}/noise
  fi
fi
if [ $sigma != 0 ]; then
   mv ${subjdir}/signal ${subjdir}/signal_nonoise
   sigma=`echo "${sigma} / ${dim1} " | bc -l `
   echo sigma for system noise is $sigma
   run "${POSSUMDIR}/bin/systemnoise --in=${subjdir}/signal_nonoise --out=${subjdir}/signal --sigma=${sigma}" $subjdir
fi
run "${POSSUMDIR}/bin/signal2image -i ${subjdir}/signal -o ${subjdir}/image -p ${subjdir}/pulse -a --homo" $subjdir
imrm ${subjdir}/image_mean

if [ `${FSLDIR}/bin/imtest ${subjdir}/image_homo` -eq 1 ]; then
   imrm image_abs
fi
